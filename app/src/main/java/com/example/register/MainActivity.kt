package com.example.register

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var name: EditText
    private lateinit var surname: EditText
    private lateinit var PhoneNumber: EditText
    private lateinit var PrivateNumber: EditText
    private lateinit var register: Button

    @SuppressLint("CutPasteId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        name = findViewById(R.id.FirstName)
        surname = findViewById(R.id.secondName)
        PhoneNumber = findViewById(R.id.phoneNumber)
        PrivateNumber = findViewById(R.id.privateNumber)
        register = findViewById(R.id.button)

        register.setOnClickListener {
            val FirstName = name.text.toString().trim()
            val SecondName = surname.text.toString().trim()
            val phNumber = PhoneNumber.text.toString().trim()
            val prNumber = PrivateNumber.text.toString().trim()

            if (FirstName.length <= 3) {
                name.error = "სახელი უნდა შედგებოდეს მინიმუმ 3 სიმბოლოსგან"
                return@setOnClickListener
            } else if (SecondName.length <= 5) {
                surname.error = "გვარი უნდა შედგებოდეს მინიმუმ 5 სიმბოლოსგან"
                return@setOnClickListener

            } else if (phNumber.length != 9) {
                PhoneNumber.error = "ტელეფონის ნომერი უნდა შედგებოდეს 9 ციფრისგან"
                return@setOnClickListener
            } else if (prNumber.length != 11) {
                PrivateNumber.error = "პირადი ნომერი უნდა შედგებოდეს 11 ციფრისგან"
                return@setOnClickListener


            } else {
                Toast.makeText(this, "რეგისტრაცია წარმატებით დასრულდა", Toast.LENGTH_SHORT).show()


            }
        }
    }
}